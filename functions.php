<?php
// ==============================================================================
//	Display Regular and sale price in shop page.
// ==============================================================================

add_action( 'woocommerce_after_shop_loop_item_title', 'nutrabox_add_sale_price', 15 );
function nutrabox_add_sale_price() {
    $nb_regular_price = get_post_meta( get_the_ID(), '_regular_price', true);
    if ($nb_regular_price >0) {
    	echo '<del>';
	    echo get_woocommerce_currency_symbol();
	    echo $nb_regular_price;
	    echo '.00';
	    echo '</del>';
    }    
}